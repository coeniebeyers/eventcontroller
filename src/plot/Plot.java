package plot;

import java.awt.Color;
import java.text.SimpleDateFormat;

import javax.swing.JPanel;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.StandardChartTheme;
import org.jfree.chart.axis.DateAxis;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.XYItemRenderer;
import org.jfree.chart.renderer.xy.XYLineAndShapeRenderer;
import org.jfree.data.time.Second;
import org.jfree.data.time.TimeSeries;
import org.jfree.data.time.TimeSeriesCollection;
import org.jfree.data.xy.XYDataset;
import org.jfree.ui.ApplicationFrame;
import org.jfree.ui.RectangleInsets;

public class Plot extends ApplicationFrame{

	private static TimeSeries ingredients = new TimeSeries("Ingredients");
	private static TimeSeries bakers = new TimeSeries("Bakers");
	private static TimeSeries mixers = new TimeSeries("Mixers");
	
	private static final long serialVersionUID = 1L;

	{
		ChartFactory.setChartTheme(new StandardChartTheme("JFree/Shadow", true));
	}

	public Plot(String applicationTitle){
		super(applicationTitle);		
		ChartPanel chartPanel = (ChartPanel) createPanel();
		chartPanel.setPreferredSize(new java.awt.Dimension(1000, 500));
		setContentPane(chartPanel);
	}

	private static XYDataset createDataset(){
		
		ingredients.addOrUpdate(new Second(0, 59, 7, 1, 1, 2013), 0);		
		bakers.addOrUpdate(new Second(0, 59, 7, 1, 1, 2013), 0);
		mixers.addOrUpdate(new Second(0, 59, 7, 1, 1, 2013), 0);
		
		TimeSeriesCollection dataset = new TimeSeriesCollection();
		dataset.addSeries(ingredients);
		dataset.addSeries(bakers);
		dataset.addSeries(mixers);

		return dataset;
	}

	private static JFreeChart createChart(XYDataset dataset) {

		JFreeChart chart = ChartFactory.createTimeSeriesChart(
				"Ingredients vs Bakers vs Mixers",
				"Time",
				"Ingredients(x10), Bakers, Mixers",
				dataset,
				true,
				true,
				false);

		chart.setBackgroundPaint(Color.white);

		XYPlot plot = (XYPlot) chart.getPlot();
		plot.setBackgroundPaint(Color.lightGray);
		plot.setDomainGridlinePaint(Color.white);
		plot.setRangeGridlinePaint(Color.white);
		plot.setAxisOffset(new RectangleInsets(5.0, 5.0, 5.0, 5.0));
		plot.setDomainCrosshairVisible(true);
		plot.setRangeCrosshairVisible(true);

		XYItemRenderer r = plot.getRenderer();
		if (r instanceof XYLineAndShapeRenderer) {
			XYLineAndShapeRenderer renderer = (XYLineAndShapeRenderer) r;
			renderer.setBaseShapesVisible(true);
			renderer.setBaseShapesFilled(true);
			renderer.setDrawSeriesLineAsPath(true);
		}

		DateAxis axis = (DateAxis) plot.getDomainAxis();
		axis.setDateFormatOverride(new SimpleDateFormat("h:m:s"));
//		axis.setTickUnit(new DateTickUnit(DateTickUnitType.SECOND, 15));
		
		return chart;

	}

	public static JPanel createPanel() {
		JFreeChart chart = createChart(createDataset());
		ChartPanel panel = new ChartPanel(chart);
		panel.setFillZoomRectangle(true);
		panel.setMouseWheelEnabled(true);
		return panel;
	}

	public static TimeSeries getIngredients() {
		return ingredients;
	}

	public static void setIngredients(TimeSeries ingredients) {
		Plot.ingredients = ingredients;
	}

	public static TimeSeries getBakers() {
		return bakers;
	}

	public static void setBakers(TimeSeries bakers) {
		Plot.bakers = bakers;
	}

	/**
	 * @return the mixers
	 */
	public static TimeSeries getMixers() {
		return mixers;
	}

	/**
	 * @param mixers the mixers to set
	 */
	public static void setMixers(TimeSeries mixers) {
		Plot.mixers = mixers;
	}

}
