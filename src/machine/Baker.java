package machine;

import ingredients.ConsumeIngredients;

import java.util.Timer;

import support.Clock;
import support.Time;

public class Baker{

	private long executionTime;
	private double ingredientConsumption;
	private Time startTime;
	private Time completionTime;
	private volatile boolean isRunning;
	private Object lock;
	private int id;

	public Baker(){
		this.isRunning = false;
		this.executionTime = 2*60*1000;
		this.ingredientConsumption = 20;
		this.startTime = new Time(8, 30, 0);
		this.lock = new Object();
	}

	public Baker(long executionTime, double ingredientConsumption, int hours, int minutes, int seconds){
		this.isRunning = false;
		this.executionTime = executionTime;
		this.ingredientConsumption = ingredientConsumption;
		this.startTime = new Time(hours, minutes, seconds);
		this.lock = new Object();
	}

	public void startBaking() {
		synchronized(lock){
			if(this.isRunning == false){
				this.isRunning = true;
				ConsumeIngredients consumeIngredients = new ConsumeIngredients(this);
				Timer timer = new Timer("baker");
				timer.schedule(consumeIngredients, (long) (Clock.convertToSimulatedTime(this.executionTime)));
				this.completionTime = Clock.addTime(this.executionTime);
				System.out.println("Started baker "+this.id+", will be completed at "+this.completionTime.toString());
			}		
		}
	}

	public long getExecutionTime() {
		return this.executionTime;
	}

	public void setExecutionTime(long executionTime) {
		this.executionTime = executionTime;
	}

	public double getIngredientConsumption() {
		return this.ingredientConsumption;
	}

	public void setIngredientConsumption(double ingredientConsumption) {
		this.ingredientConsumption = ingredientConsumption;
	}

	public Time getStartTime() {
		return this.startTime;
	}

	public void setStartTime(Time startTime) {
		this.startTime = startTime;
	}	

	public Time getCompletionTime() {
		return this.completionTime;
	}

	public boolean isNotRunning() {
		synchronized(lock){
			return !this.isRunning;
		}
	}

	public void setRunningIsComplete() {
		synchronized(lock){
			this.isRunning = false;
			this.completionTime = new Time(0,0,0);
		}
	}

	public boolean isRunning() {
		synchronized(lock){
			return this.isRunning;
		}
	}

	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}
}
