package machine;

import ingredients.ProduceIngredients;

import java.util.Timer;

import support.Clock;
import support.Time;

public class Mixer{

	private long executionTime;
	private double ingredientProduction;
	private Time startTime;
	private Time completionTime;
	private volatile boolean isRunning; 
	private Object lock;
	private int id;

	public Mixer(){
		this.isRunning = false;
		this.executionTime = 1*60*1000;
		this.ingredientProduction = 40;
		this.startTime = new Time(8, 0, 0);
		this.lock = new Object();
	}

	public Mixer(long executionTime, double ingredientProduction, int hours, int minutes, int seconds){
		this.isRunning = false;
		this.executionTime = executionTime;
		this.ingredientProduction = ingredientProduction;
		this.startTime = new Time(hours, minutes, seconds);
		this.lock = new Object();
	}

	public void startMixing() {
		synchronized(lock){
			if(isRunning == false){
				isRunning = true;
				ProduceIngredients produceIngredients = new ProduceIngredients(this);
				Timer timer = new Timer("mixer");
				timer.schedule(produceIngredients, (long) (Clock.convertToSimulatedTime(executionTime)));
				completionTime = Clock.addTime(executionTime);
				System.out.println("Started mixer "+this.id+", will be completed at "+completionTime.toString());
			}		
		}
	}

	public double getExecutionTime() {
		return executionTime;
	}

	public void setExecutionTime(long executionTime) {
		this.executionTime = executionTime;
	}

	public double getIngredientProduction() {
		return ingredientProduction;
	}

	public void setIngredientProduction(double ingredientProduction) {
		this.ingredientProduction = ingredientProduction;
	}

	public Time getStartTime() {
		return startTime;
	}

	public void setStartTime(Time startTime) {
		this.startTime = startTime;
	}

	public boolean isNotRunning() {
		synchronized(lock){
			return !this.isRunning;
		}
	}

	public boolean isRunning() {
		synchronized(lock){
			return this.isRunning;
		}
	}

	public void setRunningIsComplete() {
		synchronized(lock){
			this.isRunning = false;
			this.completionTime = new Time(0,0,0);
		}
	}

	public Time getCompletionTime() {
		return this.completionTime;
	}

	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}
}
