package support;

public class Time{

	private volatile int seconds;
	private volatile int minutes;
	private volatile int hours;

	public Time(){
		this.seconds = 0;
		this.minutes = 0;
		this.hours = 0;
	}

	public Time(int hours_, int minutes_, int seconds_){
		this.seconds = seconds_;
		this.minutes = minutes_;
		this.hours = hours_;
	}

	public Time(int hours_, int minutes_, int seconds_, double timeStep_){
		this.seconds = seconds_;
		this.minutes = minutes_;
		this.hours = hours_;
	}

	public int getSeconds() {
		return seconds;
	}

	public void setSeconds(int seconds) {
		this.seconds = seconds;
	}

	public int getMinutes() {
		return minutes;
	}

	public void setMinutes(int minutes) {
		this.minutes = minutes;
	}

	public int getHours() {
		return hours;
	}

	public void setHours(int hours) {
		this.hours = hours;
	}

	public String toString(){
		return String.format("%02d:%02d:%02d", hours, minutes, seconds);
	}

	public boolean isAfterOrEqual(Time time) {

		if(hours > time.getHours()){
			return true;
		}		
		else if(hours == time.getHours()){
			if(minutes >= time.getMinutes()){
				return true;
			}
			else if(minutes == time.getMinutes()){
				if(seconds >= time.getSeconds()){
					return true;
				}
			}
		}

		return false;
	}

}
