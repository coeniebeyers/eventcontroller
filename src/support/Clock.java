package support;

import ingredients.Ingredients;

import java.util.TimerTask;

import main.EventScheduler;

import org.jfree.data.time.RegularTimePeriod;
import org.jfree.data.time.Second;

import plot.Plot;

public class Clock extends TimerTask{

//	private static int steps;	
	private static int timeStep;
	private static Time time;
	private static Object lock = new Object();

	public Clock(){
		time = new Time(0, 0, 0);
		timeStep = 15;		
	}

	public Clock(int hours, int minutes, int seconds){
		time = new Time(hours, minutes, seconds);
		timeStep = 15;
	}

	@Override
	public void run() {

		synchronized(lock){

			int seconds = time.getSeconds();
			int minutes = time.getMinutes();
			int hours = time.getHours();

			seconds += (int)timeStep;

			if(seconds >= 60 && minutes >= 59){
				seconds %= 60;
				minutes %= 59;
				hours++;
			}
			else if(seconds >= 60){
				seconds %= 60;
				minutes++;
			}			

			time.setSeconds(seconds);
			time.setMinutes(minutes);
			time.setHours(hours);

			System.out.println("Time: "+this.toString());
		}
		
		Plot.getIngredients().addOrUpdate(Clock.getPlotTime(), Ingredients.getLevel()/10);
		Plot.getBakers().addOrUpdate(Clock.getPlotTime(), EventScheduler.getBakerCount());
		Plot.getMixers().addOrUpdate(Clock.getPlotTime(), EventScheduler.getMixerCount());	

//		steps++;
//		if(steps >= 512){
//			EventScheduler.getInstance().cancel();
//			this.cancel();
//		}
	}	

	public static double convertToSimulatedTime(long ms){		
		return ms/timeStep;
	}

	public static final Time getTime(){
		synchronized(lock){
			final Time finalTime = time;
			return finalTime;
		}
	}

	public static Time addTime(long ms){
		
		synchronized(lock){
			int seconds = time.getSeconds() + (int) (ms/1000);
			int minutes = time.getMinutes();
			int hours = time.getHours();

			while(seconds >= 60){
				seconds -= 60;
				minutes++;
			}
		
			return new Time(hours, minutes, seconds);
		}
	}

	public int getSeconds(){
		return time.getSeconds();
	}

	public int getMinutes(){
		return time.getMinutes();
	}

	public int getHours(){
		return time.getHours();
	}

	public static void setTimeStep(int timeStep_){
		timeStep = timeStep_;
	}

	public static int getTimeStep(){
		return timeStep;
	}

	public String toString(){
		return time.toString();
	}

	public static RegularTimePeriod getPlotTime() {
		synchronized(lock){
			return new Second(time.getSeconds()-Clock.getTimeStep(), time.getMinutes(), time.getHours(), 1,1,2013);
		}		
	}
}
