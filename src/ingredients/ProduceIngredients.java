package ingredients;

import java.util.TimerTask;

import machine.Mixer;

public class ProduceIngredients extends TimerTask{

	private Mixer mixer;

	public ProduceIngredients(Mixer mixer){
		this.mixer = mixer;
	}
	
	@Override
	public void run() {
		Ingredients.addIngredients(mixer.getIngredientProduction());
		this.mixer.setRunningIsComplete();
		System.out.println("Mixer "+mixer.getId()+" is complete.");
	}
}
