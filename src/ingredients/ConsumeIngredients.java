package ingredients;

import java.util.TimerTask;

import machine.Baker;

public class ConsumeIngredients extends TimerTask{

	private Baker baker;

	public ConsumeIngredients(Baker baker){
		this.baker = baker;
		Ingredients.removeIngredients(baker.getIngredientConsumption());		
	}
	
	@Override
	public void run() {
		this.baker.setRunningIsComplete();
		System.out.println("Baker "+baker.getId()+" is complete.");
	}
}
