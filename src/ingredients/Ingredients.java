package ingredients;


public class Ingredients {

	private static volatile double level;
	private static Object lock = new Object();
	
	public Ingredients(double level_){
		level = level_;
	}
	
	public static double getLevel() {
		return level;
	}

	public static void addIngredients(double addLevel){
		synchronized(lock){
			level += addLevel;
//			Plot.getIngredients().addOrUpdate(Clock.getPlotTime(), level);
		}
	}	
	
	public static void removeIngredients(double removeLevel){
		synchronized(lock){
			level -= removeLevel;
//			Plot.getIngredients().addOrUpdate(Clock.getPlotTime(), level);
		}
	}	
}
