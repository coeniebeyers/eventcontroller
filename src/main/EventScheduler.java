package main;

import ingredients.Ingredients;

import java.util.HashMap;
import java.util.TimerTask;

import machine.Baker;
import machine.Mixer;
import support.Clock;

public class EventScheduler extends TimerTask{

	private static EventScheduler instance;
	
	private static double maxIngredientLevel;

	private static HashMap<Integer, Baker> bakingMachines;
	private static HashMap<Integer, Mixer> mixingMachines;

	public EventScheduler(){
		mixingMachines = new HashMap<Integer, Mixer>();
		bakingMachines = new HashMap<Integer, Baker>();
		maxIngredientLevel = 40;
	}

	public EventScheduler(double level, double maxIngredientLevel_){
		mixingMachines = new HashMap<Integer, Mixer>();
		bakingMachines = new HashMap<Integer, Baker>();
		maxIngredientLevel = maxIngredientLevel_;	
	}

	@Override
	public void run() {
		
		for(Mixer mixer : mixingMachines.values()){
			if(Clock.getTime().isAfterOrEqual(mixer.getStartTime())
					&& mixer.isNotRunning()
					&& mixer.getIngredientProduction() + getFutureIngredientLevel() <= maxIngredientLevel){
				mixer.startMixing();
			}
		}

		for(Baker baker : bakingMachines.values()){
			if(Clock.getTime().isAfterOrEqual(baker.getStartTime())
					&& baker.isNotRunning()
					&& Ingredients.getLevel() - baker.getIngredientConsumption() >= 0){
				baker.startBaking();
			}
		}
	}
	
	public static int getBakerCount(){
		int bakerCount = 0;
		for(Baker baker : bakingMachines.values()){
			if(baker.isRunning()){
				bakerCount++;
			}
		}
		return bakerCount;
	}

	public double getFutureIngredientLevel(){
		double futureLevel = Ingredients.getLevel();
		for(Mixer mixer : mixingMachines.values()){
			if(mixer.isRunning()){
				futureLevel += mixer.getIngredientProduction();
			}
		}
		return futureLevel;
	}

	public void registerNewBakingMachine(int id, Baker newMachine){
		bakingMachines.put(id, newMachine);
		newMachine.setId(id);
	}

	public void registerNewMixingMachine(int id, Mixer newMachine){
		mixingMachines.put(id, newMachine);
		newMachine.setId(id);
	}

	public static double getMaxIngredientLevel() {
		return maxIngredientLevel;
	}

	public static void setMaxIngredientLevel(double maxIngredientLevel_) {
		maxIngredientLevel = maxIngredientLevel_;
	}

	public static Number getMixerCount() {
		int mixerCount = 0;
		for(Mixer mixer: mixingMachines.values()){
			if(mixer.isRunning()){
				mixerCount++;
			}
		}
		return mixerCount;
	}

	public static EventScheduler getInstance() {
		return instance;
	}

	public static void setInstance(EventScheduler instance) {
		EventScheduler.instance = instance;
	}
}
