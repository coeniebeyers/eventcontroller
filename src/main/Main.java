package main;

import java.util.Timer;

import machine.Baker;
import machine.Mixer;

import org.jfree.ui.RefineryUtilities;

import plot.Plot;
import support.Clock;
import support.Time;

public class Main {

	public static void main(String args[]){
		
		Mixer mixer1 = new Mixer();
		mixer1.setStartTime(new Time(8,0,0));
		Mixer mixer2 = new Mixer();
		mixer2.setStartTime(new Time(8,0,0));
		Mixer mixer3 = new Mixer();
		mixer3.setStartTime(new Time(8,0,0));
		
		Baker baker1 = new Baker();
		baker1.setStartTime(new Time(8,3,0));
		Baker baker2 = new Baker();
		baker2.setStartTime(new Time(8,3,0));
		Baker baker3 = new Baker();
		baker3.setStartTime(new Time(8,3,0));
		Baker baker4 = new Baker();
		baker4.setStartTime(new Time(8,3,0));
		Baker baker5 = new Baker();
		baker5.setStartTime(new Time(8,3,0));
		Baker baker6 = new Baker();
		baker6.setStartTime(new Time(8,3,0));
		Baker baker7 = new Baker();
		baker7.setStartTime(new Time(8,3,0));
		Baker baker8 = new Baker();
		baker8.setStartTime(new Time(8,3,0));
		Baker baker9 = new Baker();
		baker9.setStartTime(new Time(8,3,0));
		Baker baker10 = new Baker();
		baker10.setStartTime(new Time(8,3,0));
		
		// Set up the timer
		Clock clock = new Clock(7, 59, 0);
		Clock.setTimeStep(15);
		// Start the timer
		Timer timer = new Timer("clock");		
		timer.schedule(clock, 0, 1000);
		
		//Start the event scheduler
		EventScheduler eventScheduler = new EventScheduler();
		EventScheduler.setInstance(eventScheduler);
		EventScheduler.setMaxIngredientLevel(40);
		eventScheduler.registerNewMixingMachine(0, mixer1);
//		eventScheduler.registerNewMixingMachine(1, mixer2);
//		eventScheduler.registerNewMixingMachine(2, mixer3);
		
		eventScheduler.registerNewBakingMachine(0, baker1);
//		eventScheduler.registerNewBakingMachine(1, baker2);
//		eventScheduler.registerNewBakingMachine(2, baker3);
//		eventScheduler.registerNewBakingMachine(3, baker4);
//		eventScheduler.registerNewBakingMachine(4, baker5);
//		eventScheduler.registerNewBakingMachine(5, baker6);
//		eventScheduler.registerNewBakingMachine(6, baker7);
//		eventScheduler.registerNewBakingMachine(7, baker8);
//		eventScheduler.registerNewBakingMachine(8, baker9);
//		eventScheduler.registerNewBakingMachine(9, baker10);

		Timer event = new Timer("event");
		event.schedule(eventScheduler, 0, 100);
		
		// Plot data
		Plot plot = new Plot("Cadburys");
		plot.pack();
		RefineryUtilities.centerFrameOnScreen(plot);
		plot.setVisible(true);
	}	
}
